# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=10000
SAVEHIST=10000
setopt autocd extendedglob
bindkey -e
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/primavipul/.zshrc'

autoload -Uz compinit
compinit
_comp_options+=(globdots)

# End of lines added by compinstall
# # PowerLevel 10 K Configuration
# antigen theme romkatv/powerlevel10k
# antigen apply

### Added by Zinit's installer
if [[ ! -f $HOME/.local/share/zinit/zinit.git/zinit.zsh ]]; then
    print -P "%F{33} %F{220}Installing %F{33}ZDHARMA-CONTINUUM%F{220} Initiative Plugin Manager (%F{33}zdharma-continuum/zinit%F{220})…%f"
    command mkdir -p "$HOME/.local/share/zinit" && command chmod g-rwX "$HOME/.local/share/zinit"
    command git clone https://github.com/zdharma-continuum/zinit "$HOME/.local/share/zinit/zinit.git" && \
        print -P "%F{33} %F{34}Installation successful.%f%b" || \
        print -P "%F{160} The clone has failed.%f%b"
fi

source "$HOME/.local/share/zinit/zinit.git/zinit.zsh"
autoload -Uz _zinit
(( ${+_comps} )) && _comps[zinit]=_zinit

# Load a few important annexes, without Turbo
# (this is currently required for annexes)
zinit light-mode for \
    zdharma-continuum/zinit-annex-as-monitor \
    zdharma-continuum/zinit-annex-bin-gem-node \
    zdharma-continuum/zinit-annex-patch-dl \
    zdharma-continuum/zinit-annex-rust

### End of Zinit's installer chunk
# zinit light marlonrichert/zsh-autocomplete
zinit light wting/autojump
# zinit light chrissicool/zsh-bash
zinit ice depth=1; zinit light romkatv/powerlevel10k
# zinit light Doc0x1/p10k-promptconfig
P10K_PROMPT=("nnn")
zinit light  marlonrichert/zsh-autocomplete
# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh


alias nnn="nnn -Ddca"


# youtube-dl
#alias yta-aac="youtube-dl --extract-audio --audio-format aac "
#alias yta-best="youtube-dl --extract-audio --audio-format best "
#alias yta-flac="youtube-dl --extract-audio --audio-format flac "
#alias yta-m4a="youtube-dl --extract-audio --audio-format m4a "
#alias yta-mp3="youtube-dl --extract-audio --audio-format mp3 "
#alias yta-opus="youtube-dl --extract-audio --audio-format opus "
#alias yta-vorbis="youtube-dl --extract-audio --audio-format vorbis "
#alias yta-wav="youtube-dl --extract-audio --audio-format wav "
#alias ytv-best="youtube-dl -f bestvideo+bestaudio "
#alias yt-audio-and-art="youtube-dl --extract-audio --add-metadata --xattrs --embed-thumbnail --audio-quality 0 --audio-format mp3"
#alias yt-video-and-art="youtube-dl --merge-output-format mp4 -f "bestvideo+bestaudio[ext=m4a]/best" --embed-thumbnail --add-metadata"

alias yta-aac="yt-dlp --extract-audio --audio-format aac "
alias yta-best="yt-dlp --extract-audio --audio-format best "
alias yta-flac="yt-dlp --extract-audio --audio-format flac "
alias yta-m4a="yt-dlp --extract-audio --audio-format m4a "
alias yta-mp3="yt-dlp --extract-audio --audio-format mp3 "
alias yta-opus="yt-dlp --extract-audio --audio-format opus "
alias yta-vorbis="yt-dlp --extract-audio --audio-format vorbis "
alias yta-wav="yt-dlp --extract-audio --audio-format wav "
alias ytv-best="yt-dlp -f bestvideo+bestaudio "
alias yt-audio-and-art="yt-dlp --extract-audio --add-metadata --xattrs --embed-thumbnail --audio-quality 0 --audio-format mp3"
# alias yt-video-and-art="yt-dlp --merge-output-format mp4 -f "bestvideo+bestaudio[ext=m4a]/best" --embed-thumbnail --add-metadata"

# termbin
alias tb="~/.scripts/termbin"
alias aptin="sudo aptitude install"
alias aptrem="sudo aptitude remove"
alias aptser="aptitude search"
alias pixterm300="pixterm -tc 330 -tr 330 -s 2"
alias ls="exa -lgh --group-directories-first --icons"
alias la="exa -lgha --group-directories-first --icons"
#set -o vi


# source /usr/share/fzf-tab-completion/bash/fzf-bash-completion.sh
# bind -x '"\t": fzf_bash_completion'
export PATH=$PATH:/usr/local/go/bin
export PATH=$PATH:$HOME/.local/bin/

######
##nnn plugins
#######
export NNN_PLUG='f:finder;o:fzopen;p:preview-tabbed;v:vidthumb;n:nuke;b:bulknew'
export NNN_OPENER=$HOME/.config/nnn/plugins/nuke
export NNN_FIFO=/tmp/nnn.fifo
[ -f ~/.fzf.bash ] && source ~/.fzf.bash
# colorscript random
#. "$HOME/.cargo/env"
# HISTSIZE=50000
# HISTFILESIZE=50000

# Add this line to your .bashrc or a shell script.
# source ~/.cache/wal/colors.sh
# bat light theme
export BAT_THEME="OneHalfLight"
# bat dark theme
export BAT_THEME="OneHalfDark"
#export editor env variable
export SUDOEDITOR="vim"
export EDITOR="emacs"
