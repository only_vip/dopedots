#!/bin/bash
cd /home/betatwo/mpr

cd bat-cat-bin
makedeb --printsrcinfo &> .SRCINFO
git add .
git commit -m "minor update"
git push
cd ..
cd cbonsai-git
makedeb --printsrcinfo &> .SRCINFO
git add .
git commit -m "minor update"
git push
cd ..
 cd chadwm-git
 makedeb --printsrcinfo &> .SRCINFO
 git add .
 git commit -m "minor update"
 git push
cd ..
cd compton-tryone-git
makedeb --printsrcinfo &> .SRCINFO
git add .
git commit -m "minor update"
git push
cd ..
cd dunst-bin
makedeb --printsrcinfo &> .SRCINFO
git add .
git commit -m "minor update"
git push
cd dunst-git
makedeb --printsrcinfo &> .SRCINFO
git add .
git commit -m "minor update"
git push
cd ..
cd elementary-icons
makedeb --printsrcinfo &> .SRCINFO
git add .
git commit -m "minor update"
git push
cd ..
cd elementary-stylesheet
makedeb --printsrcinfo &> .SRCINFO
git add .
git commit -m "minor update"
git push
cd ..
cd exa-bin
makedeb --printsrcinfo &> .SRCINFO
git add .
git commit -m "minor update"
git push
cd ..
cd foot-bin
makedeb --printsrcinfo &> .SRCINFO
git add .
git commit -m "minor update"
git push
cd ..
cd fzf-tab-completion-git
makedeb --printsrcinfo &> .SRCINFO
git add .
git commit -m "minor update"
git push
cd ..
cd jgmenu-git
makedeb --printsrcinfo &> .SRCINFO
git add .
git commit -m "minor update"
git push
cd ..
cd libtllist-dev-bin
makedeb --printsrcinfo &> .SRCINFO
git add .
git commit -m "minor update"
git push
cd ..
cd lite-xl
makedeb --printsrcinfo &> .SRCINFO
git add .
git commit -m "minor update"
git push
cd ..
cd neofetch-git
makedeb --printsrcinfo &> .SRCINFO
git add .
git commit -m "minor update"
git push
cd ..
cd neovim-git
makedeb --printsrcinfo &> .SRCINFO
git add .
git commit -m "minor update"
git push
cd ..
cd nerd-fonts-inconsolata
makedeb --printsrcinfo &> .SRCINFO
git add .
git commit -m "minor update"
git push
cd ..
cd nerd-fonts-jetbrains-mono
makedeb --printsrcinfo &> .SRCINFO
git add .
git push
cd ..
cd nerd-fonts-ricty
makedeb --printsrcinfo &> .SRCINFO
git add .
git commit -m "minor update"
git push
cd ..
cd nerd-fonts-victor-mono
makedeb --printsrcinfo &> .SRCINFO
git add .
git commit -m "minor update"
git push
cd ..
cd nnn-git
makedeb --printsrcinfo &> .SRCINFO
git add .
git commit -m "minor update"
git push
cd ..
cd ntfd
makedeb --printsrcinfo &> .SRCINFO
git add .
git commit -m "minor update"
git push
cd ..
cd oranchelo-icon-theme
makedeb --printsrcinfo &> .SRCINFO
git add .
git commit -m "minor update"
git push
cd ..
cd picom-git
makedeb --printsrcinfo &> .SRCINFO
git add .
git commit -m "minor update"
git push
cd ..
cd picom-jonaburg-git
makedeb --printsrcinfo &> .SRCINFO
git add .
git commit -m "minor update"
git push
cd ..
cd planner
makedeb --printsrcinfo &> .SRCINFO
git add .
git commit -m "minor update"
git push
cd ..
cd pokemonsay-newgenerations-git
makedeb --printsrcinfo &> .SRCINFO
git add .
git commit -m "minor update"
git push
cd ..
cd  polybar
makedeb --printsrcinfo &> .SRCINFO
git add .
git commit -m "minor update"
git push
cd ..
cd reproc
makedeb --printsrcinfo &> .SRCINFO
git add .
git commit -m "minor update"
git push
cd ..
cd rl-custom-function-git
makedeb --printsrcinfo &> .SRCINFO
git add .
git commit -m "minor update"
git push
cd ..
cd rofi
makedeb --printsrcinfo &> .SRCINFO
git add .
git commit -m "minor update"
git push
cd ..
cd rofi-git
makedeb --printsrcinfo &> .SRCINFO
git add .
git commit -m "minor update"
git push
cd ..
cd rsfetch-git
makedeb --printsrcinfo &> .SRCINFO
git add .
git commit -m "minor update"
git push
cd ..
cd rxvt-unicode
makedeb --printsrcinfo &> .SRCINFO
git add .
git commit -m "minor update"
git push
cd ..
cd rxvt-unicode-256color
makedeb --printsrcinfo &> .SRCINFO
git add .
git commit -m "minor update"
git push
cd ..
cd rxvt-unicode-256color-unicode3
makedeb --printsrcinfo &> .SRCINFO
git add .
git commit -m "minor update"
git push
cd ..
cd shell-color-scripts
makedeb --printsrcinfo &> .SRCINFO
git add .
git commit -m "minor update"
git push
cd ..
cd siji-git
makedeb --printsrcinfo &> .SRCINFO
git add .
git commit -m "minor update"
git push
cd ..
cd spotify-adblock-git
makedeb --printsrcinfo &> .SRCINFO
git add .
git commit -m "minor update"
git push
cd ..
cd st-siduck76-git
makedeb --printsrcinfo &> .SRCINFO
git add .
git commit -m "minor update"
git push
cd ..
cd starship-bin
makedeb --printsrcinfo &> .SRCINFO
git add .
git commit -m "minor update"
git push
cd ..
cd ttf-migu
makedeb --printsrcinfo &> .SRCINFO
git add .
git commit -m "minor update"
git push
cd ..
cd ttf-weather-icons-bin
makedeb --printsrcinfo &> .SRCINFO
git add .
git commit -m "minor update"
git push
cd ..
cd ungoogled-chromium-linchrome-bin
makedeb --printsrcinfo &> .SRCINFO
git add .
git commit -m "minor update"
git push
cd ..
cd urxvt-config-git
makedeb --printsrcinfo &> .SRCINFO
git add .
git commit -m "minor update"
git push
cd ..
cd xfwm-effects
makedeb --printsrcinfo &> .SRCINFO
git add .
git commit -m "minor update"
git push
cd ..
cd yt-dlp-bin
makedeb --printsrcinfo &> .SRCINFO
git add .
git commit -m "minor update"
git push
cd ..
cd ytfzf
makedeb --printsrcinfo &> .SRCINFO
git add .
git commit -m "minor update"
git push
cd ..
cd zentile-bin
makedeb --printsrcinfo &> .SRCINFO
git add .
git commit -m "minor update"
git push
cd ..
