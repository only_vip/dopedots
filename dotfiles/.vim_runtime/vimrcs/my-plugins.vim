" Specify a directory for plugins
" - For Neovim: stdpath('data') . '/plugged'
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.vim/plugged')
    " Any valid git URL is allowed
    "Plug 'https://github.com/junegunn/vim-github-dashboard.git'
    " On-demand loading
    " Using a tagged release; wildcard allowed (requires git 1.9.2 or above)
    Plug 'fatih/vim-go', { 'tag': '*' }
    "add icons
    Plug 'ryanoasis/vim-devicons'
    Plug 'kien/rainbow_parentheses.vim'
    Plug 'mhinz/vim-startify'
    Plug 'vim-airline/vim-airline'
    Plug 'vim-utils/vim-man'
    Plug 'chun-yang/auto-pairs'
    Plug 'chrisbra/colorizer'
    Plug 'ajh17/vimcompletesme'
    Plug 'lilydjwg/colorizer'
    Plug 'tpope/vim-surround'
    Plug 'tpope/vim-vinegar'
    Plug 'mcchrish/nnn.vim'
    Plug 'vim-latex/vim-latex'
    Plug 'brennier/quicktex'
    "" Plug 'neoclide/coc.nvim', {'branch': 'release'}
    Plug 'honza/vim-snippets'
    Plug 'dylanaraps/wal.vim'
    Plug 'baskerville/vim-sxhkdrc'
    Plug 'vim-scripts/DrawIt'
    Plug 'tpope/vim-commentary'
    "Plug 'francoiscabrol/ranger.vim'
    "Plug 'vim-airline/vim-airline-themes'
    "Plug 'sheerun/vim-polyglot'
    "Plug 'vitalk/vim-simple-todo'
    "Plug 'itchyny/lightline.vim'
    "Plug 'tpope/vim-fugitive'
    "Plug 'airblade/vim-gitgutter'
    "Plug 'suan/vim-instant-markdown'
    "Plug 'chiel92/vim-autoformat'
    "Plug 'ludovicchabant/vim-gutentags'
    " Plug 'NLKNguyen/papercolor-theme'
    " Plug 'vim-scripts/proton'
    " Plug 'rakr/vim-one'
    " Plug 'sainnhe/everforest'
    " Plug 'turquoise-hexagon/fruity'
    " Plug 'lervag/vimtex'
    " Plug 'sirver/ultisnips'
    " Plug 'KeitaNakamura/tex-conceal.vim'
     Plug 'tpope/vim-repeat'

call plug#end()

"colorscheme monokai-chris
"colorscheme PaperColor
"set background=dark
colorscheme  wal
