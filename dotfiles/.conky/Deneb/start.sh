#!/bin/bash

killall conky
sleep 2s

conky -c $HOME/.conky/Deneb/Deneb.conf &> /dev/null &
