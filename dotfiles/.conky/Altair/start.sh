#!/bin/bash

killall conky
sleep 2s

conky -c $HOME/.conky/Altair/Altair.conf &> /dev/null &
