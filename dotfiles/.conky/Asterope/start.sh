#!/bin/bash

killall conky
sleep 2s

conky -c $HOME/.conky/Asterope/Asterope.conf &> /dev/null &
