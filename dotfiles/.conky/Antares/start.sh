#!/bin/bash

killall conky
sleep 2s

conky -c $HOME/.conky/Antares/Antares.conf &> /dev/null &
