#!/bin/bash

killall conky
sleep 2s

conky -c $HOME/.conky/Albireo/Albireo.conf &> /dev/null &
