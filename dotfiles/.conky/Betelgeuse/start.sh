#!/bin/bash

killall conky
sleep 2s

conky -c $HOME/.conky/Betelgeuse/Betelgeuse.conf &> /dev/null &
