#!/bin/bash

killall conky
sleep 2s

conky -c $HOME/.conky/Graffias/Graffias.conf &> /dev/null &
