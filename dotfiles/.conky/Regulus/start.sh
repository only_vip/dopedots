#!/bin/bash

killall conky
sleep 2s

conky -c $HOME/.conky/Regulus/Regulus.conf &> /dev/null &
