#!/bin/bash

killall conky
sleep 2s

conky -c $HOME/.conky/Bellatrix/Bellatrix.conf &> /dev/null &
