#!/bin/sh

if [ x"${DESKTOP_SESSION##*/}" = x"dwm" ]; then 
   # No widgets enabled!
   exit 0
fi
if [ x"${DESKTOP_SESSION##*/}" = x"lightdm-xsession" ]; then 
   sleep 20s
   killall conky
   cd "$HOME/.conky/Betelgeuse"
   conky -c "$HOME/.conky/Betelgeuse/Betelgeuse.conf" &
   exit 0
fi
if [ x"${DESKTOP_SESSION##*/}" = x"bspwm" ]; then 
   sleep 20s
   killall conky
   cd "$HOME/.conky/Betelgeuse"
   conky -c "$HOME/.conky/Betelgeuse/Betelgeuse.conf" &
   exit 0
fi
