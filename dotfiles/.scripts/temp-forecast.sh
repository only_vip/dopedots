#/bin/bash
weather_output=$(~/.scripts/weather.sh | tr '\n' ' ' | cut -d ' ' -f '18,22' && ~/.scripts/forecast.sh | sed '3!d' | cut -d ' ' -f '2,12' | sed -e 's,Temp:,,g;s,°C,,g')
echo $weather_output | tr '\n' ''

