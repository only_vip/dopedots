#!/bin/bash
ApiKey=""
CityId=""
full_fore_data=$(curl -s "https://api.openweathermap.org/data/2.5/forecast?&id=${CityId}&units=metric&appid=${ApiKey}")
echo ${full_fore_data} &> /tmp/forecastweather.json

                icons_forecast=$(echo ${full_fore_data} | jq -r .list[1].weather[] | jq -r .icon | tr '\n' ' ')
                iconval_forecast=${icons_forecast%?}
                case $iconval_forecast in
                    01*) icons_forecast="";;      #clear sky
                    02*) icons_forecast="";;      #few clouds
                    03*) icons_forecast="";;      #scattered clouds
                    04*) icons_forecast="";;      #broken clouds
                    09*) icons_forecast="";;      #shower rain
                    10*) icons_forecast="";;     #rain
                    11*) icons_forecast="";;      #thunderstorm
                    13*) icons_forecast="";;      #snow
                    50*) icons_forecast="";;      #mist
                esac


                icons_current=$(echo ${full_fore_data} | jq -r .list[0].weather[] | jq -r .icon | tr '\n' ' ')
                iconval_current=${icons_current%?}
                case $iconval_current in
                    01*) icons_current="";;      #clear sky
                    02*) icons_current="";;       #few clouds
                    03*) icons_current="";;       #scattered clouds
                    04*) icons_current="";;       #broken clouds
                    09*) icons_current="";;       #shower rain
                    10*) icons_current="";;      #rain
                    11*) icons_current="";;       #thunderstorm
                    13*) icons_current="";;       #snow
                    50*) icons_current="";;       #mist
                esac

                temp_now=$(echo ${full_fore_data} | jq -r .list[0].main.temp)
                temp_fore=$(echo ${full_fore_data} | jq -r .list[1].main.temp)
                if [ ${temp_fore%.*} -gt ${temp_now%.*} ]; then
                    echo "${icons_current} ${temp_now%.*} 勤 ${icons_forecast} ${temp_fore%.*}"
                elif [ ${temp_fore%.*} -lt ${temp_now%.*} ]; then
                    echo "${icons_current} ${temp_now%.*} 免 ${icons_forecast} ${temp_fore%.*}"
                else
                    echo "${icons_current} ${temp_now%.*} 勉 ${icons_forecast} ${temp_fore%.*}"
                fi

                #if (( $(bc <<< "${temp_fore} > ${temp_now}") )); then
                #echo "${icons_current} ${temp_now} 免 ${icons_forecast} ${temp_fore}"
                #elif (( $(bc <<< "${temp_fore} < ${temp_now}") )); then
                   #echo "${icons_current} ${temp_now} 勤 ${icons_forecast} ${temp_fore}"
                #else
                   #echo "${icons_current} ${temp_now} 勉 ${icons_forecast} ${temp_fore}"
                #fi

#
                #outis one
                ##!/bin/bash
#
#fetch() {
    #local api_key="036fc682ac3fb1363862cfd211d9a5e4"
    #local city_id="5344157"
#
    #curl -s -G "https://api.openweathermap.org/data/2.5/weather" \
        #--data-urlencode id=${city_id} \
        #--data-urlencode units=metrics \
        #--data-urlencode appid=${api_key} \
        #--output /tmp/current_weather.json
#
    #curl -s -G "https://api.openweathermap.org/data/2.5/forecast" \
        #--data-urlencode id=${city_id} \
        #--data-urlencode units=metrics \
        #--data-urlencode appid=${api_key} \
        #--output /tmp/forecast_weather.json
#}
#
#render() {
    #local current="$(jq -j '.weather[].icon, " ", .main.temp' < /tmp/current_weather.json)"
    #local forecast="$(jq -j '.list[0] | .weather[].icon, " ", .main.temp' < /tmp/forecast_weather.json)"
#
    #printf "${current} | ${forecast}" | _icons_to_fontawesome
#}
#
#_icons_to_fontawesome() {
    #sed -e 's/01./滛/g' \
        #-e 's/02.//g' \
        #-e 's/03.//g' \
        #-e 's/04.//g' \
        #-e 's/09./晴/g' \
        #-e 's/10./殺/g' \
        #-e 's/11./ﭼ/g' \
        #-e 's/13.//g' \
        #-e 's/50.//g'
#}
#
#fetch && render
