#!/bin/bash
  mkdir ~/Projects
  mkdir ~/Projects/Code
  mkdir ~/Pics
  mkdir ~/.pix
  mkdir ~/.pix/wall
  mkdir ~/.walls
  mkdir ~/.mpr
  mkdir ~/Documents/roam
  mkdir ~/Documents/gtd
  # cd ~/Projects/Code
  # git clone https://gitlab.com/only_vip/dopedots.git

sudo aptitude remove vim-tiny vim-common asunder bluetooth clementine simple-scan gscan2pdf hexchat lbreakout2 gnome-mahjongg mc mc-data nomacs nomacs-l10n thunderbird transmission-gtk transmission-common xfburn gthumb vlc rtorrent blueman lazpaint asunder pdfarranger -y

sudo aptitude install fonts-noto-color-emoji fonts-noto-color-emoji fonts-symbola node-emojis-list ttf-ancient-fonts-symbola fonts-fork-awesome fonts-powerline fonts-roboto fonts-roboto-fontface fonts-ubuntu ttf-ubuntu-font-family fonts-font-awesome fonts-fork-awesome fonts-material-design-icons-iconfont vim cmake lxappearance fzf w3m w3m-img lolcat arandr nitrogen sxiv mpv x11-utils mpd mpc ncmpcpp pylint dmenu netcat jq ffmpeg caca-utils chafa libsixel1 flameshot libsixel-bin qbittorrent uget network-manager git curl wget tree libreadline-dev xattr zathura zathura-cb zathura-djvu zathura-pdf-poppler zathura-ps python3-setuptools python3-dev python3-pip atool rar moc mediainfo exiftool odt2txt rtorrent python3-wheel python3-docopt tmux python3-ueberzug ripgrep fd-find eyed3 python3-pylast lxpolkit dvisvgm roxterm atomicparsley ncat emacs emacs-gtk i3lock i3lock-fancy libtool-bin klavaro gftp pdf2svg graphviz scrot pdftk oxygencursors fonts-materialdesignicons-webfont numlockx -y
sudo apt install texlive-full 
#+begin_src
wget -qO - 'https://proget.hunterwittenborn.com/debian-feeds/makedeb.pub' | \
gpg --dearmor | \
sudo tee /usr/share/keyrings/makedeb-archive-keyring.gpg &> /dev/null
echo 'deb [signed-by=/usr/share/keyrings/makedeb-archive-keyring.gpg arch=all] https://proget.hunterwittenborn.com/ makedeb main' | \
sudo tee /etc/apt/sources.list.d/makedeb.list
sudo apt update && sudo apt install makedeb
#+end_src
#+begin_src
cd ~/.mpr
git clone https://mpr.makedeb.com/tap.git
cd tap
makedeb -si
sudo tap update
#+end_src

#+begin_src
sudo tap install libptytty-bin && sudo tap install bat-cat-bin exa-bin picom-git dunst foot-bin neofetch-git neovim-git nerd-fonts-victor-mono rofi rxvt-unicode-256color shell-color-scripts-git nerd-fonts-jetbrains-mono ttf-weather-icons-bin ungoogled-chromium-linchrome-bin yt-dlp-bin ytfzf polybar bspwm-git bsptab-git sxhkd-git lemonbar-xft-git starship-bin
#+end_src
# *** Misc stuff you can get from MPR
#+begin_src
sudo tap install rl-custom-function-git &&
sudo tap install fzf-tab-completion-git compix-git nnn-git zotero &&
sudo tap instal epy-git 
#+end_src
cp -vRf ~/Projects/Code/dopedots/dotfiles/* ~/*

pip3 install pywal colorz

fc-cache -frv

 ln -s -f ~/.tmux/.tmux.conf ~/.tmux.conf
ln -sfv ~/.cache/wal/colors.Xresources -t ~/.Xresources-themes/wal-colors.Xresources
